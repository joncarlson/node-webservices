/**
 * service.js: Routes webservice calls to the appropriate endpoints
 *
 * @author LU Web Product Team
 * @created January 2014
 */


var director = require('director'),
		winston = require('winston'),
		fs = require('fs');


/**
 * creates the router for webservices
 *
 * @return {object} [the router]
 */
exports.createRouter = function() {
	// create the router
	var router = new director.http.Router().configure({
		strict: false,
		async: true
	});

	/**
	 * JSON output handler
	 *
	 * @param  {any} output 	[the item to convert/output in JSON]
	 * @param  {object} res    	[the response]
	 */
	router.returnJSON = function(output, res) {
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end(JSON.stringify(output));
	};

	winston.info("\r\n\r\nLoading routes from: " + global.settings.services_path);

	// loop through the services_path directory and add routes from any services found
	fs.readdirSync(global.settings.services_path).forEach(function(file) {
		// parse the extension out and make the path relative
		var route = (global.settings.services_path + file.substr(0, file.indexOf('.'))).replace(/\.\//gi, "../");

		winston.info("Adding route: " + route);

		// include the new routes
		route = require(route);
		router = route.init(router);
	});

	winston.info("All routes loaded\r\n");

	// return router with all routes setup
	return router;
};