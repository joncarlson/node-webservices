/**
 * handler.js: Defines the server handler, which provides routing, error handling, etc.
 *
 * @author LU Web Product Team
 * @created January 2014
 */


var http = require('http'),
		winston = require('winston'),
		union = require('union'),
		service = require('./service'),
		fs = require('fs');

/**
 * Creates the server
 *
 * @param {int} port [port for the server to run on]
 */
exports.createServer = function(port) {
	var router = service.createRouter(), server;

	// create the server
	server = union.createServer({
		before: [
			function (req, res) {
				// dispatch the request to the router
				winston.info('Incoming request: ' + req.url);
				router.dispatch(req, res, function(err) {
					// handle any errors that come up
					if(err) {
						winston.info('404 Request not found (' + req.url + ')');

						// output the error to the client
						res.writeHead(404, { 'Content-Type': 'text/html' });
						res.end("<h2>The page you requested was not found</h2>");
					}
				});
			}
		]
	});

	// listen for requests on the given port
	if(port) {
		server.listen(port);
	}

	return server;
};

/**
 * Initialization before starting the server (loading configuration, etc.)
 *
 * @param  {object}   options  [options for server startup]
 * @param  {Function} callback [after server has started (or not), this will be fired]
 */
exports.start = function (options, callback) {
	// load server configuration
	winston.info("Loading configuration file: /config/app.json");
	global.settings = JSON.parse(fs.readFileSync('./config/app.json', encoding="ascii"));

	// create server and fire callback when it's done
	var server = exports.createServer(options.port);
	callback(null, server);
}