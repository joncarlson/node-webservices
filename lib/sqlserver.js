/**
 * sqlserver.js: Abstracts SQL Server database connections and querying
 *
 * WARNING: if you have a large number of rows you need to retrieve, you
 * should add a listener for 'row' to the request, otherwise you will kill
 * the memory. (see http://pekim.github.io/tedious/api-request.html#event_row)
 *
 * @author LU Web Product Team
 * @created January 2014
 */


var tedious = require('tedious'),
	extend = require('extend'),
	winston = require('winston'),
	ConnectionPool = require('tedious-connection-pool');

var Connection = tedious.Connection;
var Request = tedious.Request;


/**
 * create a connection pool so we can manage many connections at once
 *
 * This one will cap the number of simultaneous SQL Server connections to 10, if
 * there are more than 10 requests they will be batch processed 10 at a time.
 *
 * @type {ConnectionPool}
 */
var pool = new ConnectionPool({
	max: global.settings.database.sqlserver.max_connections,
	idleTimeoutMillis: global.settings.database.sqlserver.idle_timeout
}, {
	server: global.settings.database.sqlserver.host,
	userName: global.settings.database.sqlserver.user,
	password: global.settings.database.sqlserver.password,
	database: "www",
	options: {
		database: "www",
		port: global.settings.database.sqlserver.port,
		rowCollectionOnRequestCompletion: true,
		connectTimeout: global.settings.database.sqlserver.connection_timeout,
		requestTimeout: global.settings.database.sqlserver.request_timeout
	},
	debug: {
		packet: true,
		data: true,
		payload: true,
		token: true,
		log: true
	}
});


/**
 * creates a new SQL Server connection and sets up event handlers
 *
 * @param 	{function} 	options.success 	[successful connection callback]
 * @param 	{function} 	options.error 		[unsuccessful connection callback]
 * @param 	{boolean} 	options.debug 		[whether to show debug log messages, verbose]
 */
exports.openConnection = function(options) {
	// extend the default options with the passed in ones
	options = extend(false, {
		success: function() {},
		error: function(a) {},
		debug: false
	}, options);

	// get a connection from the pool
	pool.requestConnection(function(err, connection) {
		if(!err) {
			if(options.debug)
				winston.info("[SQL] => Connection Opened Successfully");

			// listener for connection (or connection failure)
			connection.on('connect', function(err) {
				options.success(connection); 	// fire provided success callback
			});

			// fatal errors
			connection.on('errorMessage', function(info) {
				winston.error("[SQL] => Error");
				winston.error(info);
			});

			// we'll add some more information if we're debugging
			if(options.debug) {
				// non-fatal errors
				connection.on('infoMessage', function(info) {
					winston.info("[SQL] => Info");
					winston.info(info);
				});

				// log connection closing
				connection.on('end', function() {
					winston.info("[SQL] => Connection Closed");
				});

				// general debug messages
				connection.on('debug', function(msg) {
					winston.info("[SQL] => " + msg);
				});
			}
		} else {
			winston.error("[SQL] => Connection Error");
			winston.error(err);
			options.error(err);					// fire provided error callback
		}
	});
};


/**
 * abstraction for querying SQL Server tables
 *
 * @param 	{string} 	query 			[the query to execute]
 * @param 	{function} 	options.success [callback if query is successful]
 * @param 	{function} 	options.error 	[callback if query is unsuccessful]
 */
exports.query = function(connection, query, options) {
	// extend the default options with the passed in ones
	options = extend(false, {
		success: function(a, b) {},
		error: function(a) {}
	}, options);

	// create a new request and handle events after execution
	var request = new Request(query, function(err, row_count, rows) {
		if(!err) {
			options.success(row_count, rows);			// fire the success callback with the results
		} else {
			winston.error("[SQL] => Request Failed")
			winston.error(err);
			options.error(err);							// fire the error callback with the error message
		}
	});

	// execute the query
	connection.execSql(request);
};


/**
 * closes a SQL Server connection
 */
exports.closeConnection = function(connection) {
	connection.close();
}