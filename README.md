# Node Webservices

A Node.js webservice server providing a few webservices converted from Coldfusion to Node.

Meant to be a proof of concept example for webservice creation and database access and not necessarily a production ready version.

### Setup/Installation

* Get a copy of the files

		git clone https://joncarlson@bitbucket.org/joncarlson/node-webservices.git

* Install required Node libraries

		npm install optimist 	## option parsing (for arguments)
		npm install winston 	## asynchronous logging
		npm install director 	## HTTP request routing
		npm install tedious 	## SQL Server connections
		npm install validator	## Sanitization and validation

* Update config/app.json with database connection info and then lock the file so you don't inadvertently commit login information.

		git update-index --assume-unchanged config/app.json

* Run the server in the projects root

		node server.js

### Adding a webservice

Webservices are located in the *services* folder. Copy/paste an existing webservice, rename it, and you're on your way!

For the moment, to see changes, you'll need to stop/start the server. I'm looking into ways to have it pick up changes automagically.

### Available webservices

Currently, the following webservices are available:

* Latest Blog Post (hits the DEV SQL Server)

		URL: /blog/latest/{blogID}

		Example: /blog/latest/26347 (gets latest blog post under Online Communities)

* Current Weather
* Social Media Followers