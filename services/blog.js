/**
 * blog.js: Defines the blog web service
 *
 * @author LU Web Product Team
 * @created January 2014
 */

var url = require('url'),
	winston = require('winston'),
	sqls = require('../lib/sqlserver'),
	sanitize = require('validator').sanitize,
	check = require('validator').check;

/**
 * adds routes for this webservice
 *
 * @param  {object} router [the HTTP request router]
 * @return {object}        [the HTTP request router with new routes]
 */
exports.init = function(router) {
	var conn;			// keep track of the database connection

	// add the base route for this webservice
	router.path(/\/blog/, function() {

		/**
		 * GET
		 * /blog/latest/{blogID}
		 *
		 * Returns the latest blog post and information about it
		 *
		 * @param {int} blogID [the ID of the blog, ex. Online Communities blog is 26347]
		 */
		this.get(/\/latest\/([\w|\d|\-|\_]+)/, function(blogID) {
			// if debug, we'll want to see more in the logs for debugging purposes
			var urlParts = url.parse(this.req.url, true).query;
			var debug = ("debug" in urlParts && urlParts.debug === "true") ? true : false;

			// keep track of the response
			var res = this.res;

			// create a new SQL Server connection
			sqls.openConnection({
					success: function(connection) {
						conn = connection;		// keep track of our connection
						getLatestBlogPost();
					},
					error: function(err) {
						router.returnJSON({ error: "connection failed" }, res);
					},
					debug: debug
				}
			);

			function getLatestBlogPost() {
				try {
					// make sure we received a valid blog id
					check(blogID).notNull().isInt();

					// query for the latest blog post
					sqls.query(
						conn,
						"SELECT TOP 1 LinkURL, Content, PID, OriginalID, ApprovedDate " +
						"FROM dbo.wwwPages " +
						"WHERE Active = 1 AND TextOrder > 0 AND PID = '" + sanitize(blogID).toInt() + "' " +
						"ORDER BY TextOrder ASC",
						{
							/**
							 * successful query will return a JSON object containing the latest post
							 *
							 * @param 	{int} 	rowCount 	[number of results]
							 * @param  	{array} rows     	[the results]
							 */
							success: function(rowCount, rows) {
								var post = { results: [] };		// default return

								if(rowCount > 0) {
									// add the post to the results
									post.results.push({
										link: "//www.liberty.edu/index.cfm?PID=9720&blogpid=" + rows[0].PID.value + "&id=" + rows[0].OriginalID.value,
										posted: rows[0].ApprovedDate.value,
										title: rows[0].LinkURL.value,
										content: rows[0].Content.value
									});
								}

								// output the results in JSON
								router.returnJSON(post, res);

								// make sure we close the connection
								sqls.closeConnection(conn);
							},
							error: function(err) {
								// alert client to failed query
								router.returnJSON({ error: "error retrieving results" }, res);

								// make sure we close the connection
								sqls.closeConnection(conn);
							}
						}
					);
				} catch(e) {
					// blog id failed validation, alert client
					router.returnJSON({ error: "invalid blog id" }, res);

					// make sure we close the connection
					sqls.closeConnection(conn);
				}
			}
		});

	});

	// return the new routes
	return router;
};