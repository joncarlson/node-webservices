#!/usr/bin/env node

var util = require('util'),
	path = require('path'),
	argv = require('optimist').argv,
	handler = require('./lib/handler');

// add a quick help screen so the user knows how to change some server config
var help = [
	"usage: server [options]",
	"",
	"Runs a web server providing RESTful webservices",
	"",
	"options:",
	"  -p             Port that you want the server to run on [8000]",
	"  -h, --help     You're staring at it",
].join('\n');

// if user requests the help screen, show it
if(argv.h || argv.help)
	return console.log(help);

// set the port number
var port = argv.p || 8000;

// start the server on the requested port
handler.start({
	port: port
}, function(err, server) {
	if(err) return console.log('Error starting server: ' + err.message);

	console.log("\r\nServer started on http://127.0.0.1:" + port + "\r\n");
});
